# Zore grigri
Tool for drawing grigris in Zore JDR

## Installation
* Put index.php and index.txt on a website server
* Don't forget to give rights to PHP (it changes index.txt)
* Access it !

## Required libraries
There are no frameworks. You need only PHP on your server, and keep JavaScript enable for clients.

## Contributors
* Bryan Fauquembergue

## Improvements
This project can be develop in the future. Don't hesitate to share your improvements, or to fork it ! To improve it, write an email to @Brynanum with your wanted action.