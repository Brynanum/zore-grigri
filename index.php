<?php
	// Manage variables
	$undraw=[];$drew=[];$drawn=[];
	function getVariables() {
		global $undraw; global $drew; global $drawn;
		$file=explode("\n",file_get_contents('index.txt'));
		$undraw=explode(',',$file[0]); $drew=explode(',',$file[1]); $drawn=explode(',',$file[2]);
	}
	function setVariables() {
		global $undraw; global $drew; global $drawn;
		$undraw=array_filter($undraw); $drew=array_filter($drew); $drawn=array_filter($drawn); // Remove empty elements (can be arrive)
		$file=implode(',',$undraw)."\n".implode(',',$drew)."\n".implode(',',$drawn);
		file_put_contents('index.txt',$file);
	}
	
	// Execute action
	if(isset($_GET['draw'])) {
		if($_GET['draw']%1==0 && $_GET['draw']>0 && $_GET['draw']<100) {
			getVariables();
			$drawn=array_merge($drawn,$drew);
			foreach(array_keys($drawn,'G') as $key) // greens are put back in the bag
				unset($drawn[$key]);
			$drew=[]; $green=0;
			for($i=0; $i<$_GET['draw']; $i++) {
				$sizeof=sizeof($undraw);
				if($sizeof>0) {
					$index=rand(0,sizeof($undraw)-1);
					$drew[]=$undraw[$index];
					if($undraw[$index]=='G')
						$green++;
					unset($undraw[$index]);
					$undraw=array_values($undraw);
				}
			}
			for($i=0;$i<$green;$i++) // greens are put back in the bag
				$undraw[]='G';
			setVariables();
		}
	} elseif(isset($_GET['clean'])) {
		if($_GET['clean']%1==0 && $_GET['clean']>0 && $_GET['clean']<100) {
			$undraw=[]; $drew=[]; $drawn=[];
			for($i=0;$i<$_GET['clean'];$i++)
				$undraw=array_merge($undraw,['W','W','W','W','W','R']);
			for($i=1;$i<$_GET['clean'];$i+=2)
				$undraw[]='G';
			setVariables();
		}
	} elseif(isset($_GET['change']) && ($_GET['color']=='R' || $_GET['color']=='W')) {
		getVariables();
		foreach(array_keys($drew,'G') as $key)
			unset($drew[$key]);
		$drawn=array_merge($drawn,$drew);
		$drew=[];
		if($_GET['change']==='true' && ($key=array_search($_GET['color'],$undraw))!==false) { // Exit from the bag
			$drawn[]=$undraw[$key];
			unset($undraw[$key]);
		} elseif($_GET['change']==='false' && ($key=array_search($_GET['color'],$drawn))!==false) { // Put back in the bag
			$undraw[]=$drawn[$key];
			unset($drawn[$key]);
		}
		setVariables();
	}
?><!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	    <meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	    <meta name="viewport" content="width=device-width, user-scalable=no" />
	    <meta name="distribution" content="private" />
	    <meta name="author" lang="fr" content="Bryan Fauquembergue" />
		<meta name="robots" content="noindex, nofollow, noarchive" />
		<title>Zore - Grigri</title>
		<style>
			/* Main */
			body {
				background-color: grey;
			}
			section {
				padding: 10px;
			}
			section aside {
				display: flex;
				flex-wrap: wrap;
				opacity: 1;
				transition: opacity 0.3s;
			}
		
			/* Grigris */
			.grigri {
				width: 50px;
				height: 50px;
				margin: 5px;
				border: 2px solid black;
				border-radius: 50px;
			}
			#drew .grigri {
				background-color: transparent;
				transition: background-color 0.3s;
			}
			
			/* Sections */
			#config button {
				margin: 5px;
			}
		</style>
	</head>
	<body>
		<main>
			<section id="config">
				<p>Nombre de joueurs : <input type="number" name="playerNumber" value="2" /></p>
				<p>Nombre de boules à piocher : <input type="number" name="drawNumber" value="1" /></p>
				<button onclick="draw();">Piocher</button>
				<button onclick="clean();">Remise à zéro</button>
			</section>
			<section id="undraw">
				<p>Contenu du sac :</p>
				<aside>
					<p>En cours de chargement...</p>
				</aside>
			</section>
			<section id="drew">
				<p>Contenu de la pioche :</p>
				<aside>
					<p>En cours de chargement...</p>
				</aside>
			</section>
			<section id="drawn">
				<p>Déjà sorti du sac :</p>
				<aside>
					<p>En cours de chargement...</p>
				</aside>
			</section>
		</main>
		<script>
			var undraw=[''];
			var drew=[''];
			var drawn=[''];
			var colors={'G':'green','R':'red','W':'white'};
			
			// Actualize data for each user
			function loadGrigri() {
		 		var asides=document.querySelectorAll('section aside');
		 		for(let aside of asides)
		 			aside.style.opacity=0;
		 		setTimeout(function(){
					function asideInit(type,aside) {
						aside.innerHTML='';
			 			for(let grigri of type)
			 				aside.innerHTML+='<div class="grigri" onclick="change(this);" style="background-color:'+colors[grigri]+';"></div>';
					}
					asideInit(undraw,asides[0]);
					asideInit(drew,asides[1]);
					asideInit(drawn,asides[2]);
		 			for(let aside of asides)
		 				aside.style.opacity=1;
		 		},400);
		 	}
		 	function changeVariables(data) {
		 		data=data.split("\n");
    			let dataUndraw=data[0].split(',').sort();
    			if(JSON.stringify(undraw)!=JSON.stringify(dataUndraw)) {
    				undraw=dataUndraw;
    				drew=data[1].split(',').sort();
    				drawn=data[2].split(',').sort();
    				loadGrigri();
    			}
		 	}
		 	setInterval(function(){
		 		let xhttp = new XMLHttpRequest();
  				xhttp.onreadystatechange = function() {
    				if(this.readyState == 4 && this.status == 200) {
    					changeVariables(this.responseText);
    				}
  				};
  				xhttp.open('GET','index.txt',true);
  				xhttp.send();
		 	},1000);
		 	
		 	// Realize an action asked by an user
		 	function act(url) {
		 		let xhttp = new XMLHttpRequest();
  				xhttp.open('GET',url,true);
  				xhttp.send();
		 	}
		 	function draw() {
		 		act('index.php?draw='+document.querySelector('input[name="drawNumber"]').value);
		 	}
		 	function clean() {
		 		if(confirm('Êtes-vous sûr de tout remettre à zéro ?'+"\n"+'(Ok pour remettre à zéro, Annuler pour revenir en arrière)'))
		 			act('index.php?clean='+document.querySelector('input[name="playerNumber"]').value);
		 	}
			function change(grigri) {
					act('index.php?change='+document.getElementById('undraw').contains(grigri)+'&color='+grigri.style.backgroundColor[0].toUpperCase());
			}
		</script>
	</body>
</html>
